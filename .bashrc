#!/bin/bash

export HISTCONTROL=ignoreboth:erasedups
[[ $TERM_PROGRAM == 'vscode' ]] && EDITOR='code --wait'

shopt -s autocd
stty -ixon # Desactiva Ctrl-S

source /usr/share/bash-completion/bash_completion

alias e="$EDITOR"
alias se='sudoedit'
alias ce='code'
alias o='xdg-open'
alias cp='cp -i'
alias ls='ls -N --color=auto'
alias grep='grep --color=auto'
alias man='COLUMNS=80 man'
alias clip='xclip -selection clipboard'
alias stow='stow --no-folding -d ~/.local/stow -t ~/.local'
alias sc='sudo systemctl'
command -v 'fdfind' > /dev/null && alias fd='fdfind'

complete -F _systemctl sc
_completion_loader systemctl

##### Pacman

pacman-autoremove() {
    pacman -Qtdq | sudo pacman -Rns -
}

pacman-list() {
    comm -23 <(pacman -Qqett | sort) \
             <(pacman -Qq -g base-devel -g gnome | sort | uniq)
}

##### Apt

alias sa='sudo apt'

complete -F _apt sa
_completion_loader apt

apt-list() {
    apt list --manual-installed 2>/dev/null | sed -nr 's@^([^/]+).*@\1@p' | tail -n +2
}

##### Git

alias g='git'

# https://askubuntu.com/a/1059865/840298
complete -o bashdefault -o default -o nospace -F __git_wrap__git_main g
_completion_loader git

git-prune() {
    git remote prune "${1:-origin}"
    local gone=$(git branch -vv | grep "${1:-origin}"'/.*: gone' | awk '{print $1}')
    [[ $gone ]] && git branch -D $gone
}

##### Python

alias py='ipython3 -i -c "from math import *"'
alias pip-upgrade='pipupgrade -u --pip-path /home/carlos/.local/bin/pip3'

pip-list() {
    pipdeptree -u -w silence |& grep -v '^ '
}

py-venv() {
    local path=$1 name=$(basename "$1")
    [[ -d $path ]] || {
        python3 -m venv --system-site-packages --prompt "$name" "$path"
        python3 -m ipykernel install --user --name="$name"
        local lsite=(~/.local/lib/python3*/site-packages)
        local vsite=($path/lib/python3*/site-packages)
        echo "${lsite[0]}" > "${vsite[0]}/base.pth"
    }
    source "$path"/bin/activate
}

# https://github.com/jupyter/notebook/issues/4585
__jupyter_complete_baseopts=" "

##### Prompt

_ps1_branch() {
    local repo=$(git rev-parse --show-toplevel 2> /dev/null)
    [[ $repo && $repo != $HOME ]] && {
        printf "$(git branch | sed -nr 's/^\* (.*)/\1/p')"
    }
}
_short_path() {
    echo ${PWD/$HOME/'~'} | sed -r 's:([^/]{,1})[^/]*/:\1/:g'
}

PS1='\[\e[1;32m\]$(_short_path)\[\e[1;35m\]:\[\e[1;31m\]$(_ps1_branch)\[\e[1;35m\]:\[\e[0m\] '
PROMPT_COMMAND='echo -ne "\033]0;$(_short_path | sed 's/^~$/Home/')\007"'

##### Library

for _lib in ~/.local/lib/shell/*; do
    source "$_lib"
done
unset _lib
