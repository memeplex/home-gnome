import pandas as pd

from concurrent.futures import ThreadPoolExecutor
from sqlalchemy.engine import reflection

from util import render


default_engine = None


def query(sql, macro=None, engine=None, **kwargs):
    return pd.read_sql(render(sql, macro, **kwargs), engine or default_engine)


def execute(*args, **kwargs):
    return list(iexecute(*args, **kwargs))


def iexecute(sql, macro=None, engine=None, **kwargs):
    with (engine or default_engine).connect() as conn:
        yield from conn.execute(render(sql, macro, **kwargs))


def tables(schema=None, engine=None):
    insp = reflection.Inspector.from_engine(engine or default_engine)
    return insp.get_table_names(schema=schema)


def columns(table, engine=None):
    insp = reflection.Inspector.from_engine(engine or default_engine)
    cols = insp.get_columns(table)
    cols = [{**c, 'type': c['type'].__class__.__name__} for c in cols]
    return pd.DataFrame(cols)


def asyn(fun, *args, **kwargs):
    executor = ThreadPoolExecutor(max_workers=1)
    future = executor.submit(fun, *args, **kwargs)
    executor.shutdown(wait=False)
    return future


def show_all():
    return pd.option_context('display.max_rows', None, 'display.max_columns', None)
