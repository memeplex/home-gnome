import subprocess
import jinja2
import os


jinja_init = None


def sh(cmd, input=None, capture=True, text=True, check=True):
    if input and not isinstance(input, (bytes, str)):
        input = "\n".join(input)
    return subprocess.run(
        cmd, input=input, capture_output=capture, text=text, check=check, shell=True
    ).stdout


def sh_quote(s):
    return "'" + s.replace("'", "'\\''") + "'"


def render(path_or_text, macro=None, init_environment=None, *args, **kwargs):
    loader = jinja2.FileSystemLoader(f"{os.environ['HOME']}/.local/lib/jinja")
    environment = jinja2.Environment(loader=loader)
    if jinja_init:
        jinja_init(environment)
    try:
        with open(path_or_text) as file:
            tmpl = environment.from_string(file.read())
    except (FileNotFoundError, OSError):
        tmpl = environment.from_string(path_or_text)
    if macro:
        return getattr(tmpl.module, macro)(*args, **kwargs)
    else:
        return tmpl.render(*args, **kwargs)
