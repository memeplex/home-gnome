#!/bin/bash

export EDITOR=gedit
export PATH=~/.local/bin${PATH:+:$PATH}
export MANPATH=~/.local/share/man:$MANPATH
export LD_LIBRARY_PATH=~/.local/lib${LD_LIBRARY_PATH:+:LD_LIBRARY_PATH}
export PYTHONPATH=~/.local/lib/python${PYHTONPATH:+:PYHTONPATH}
export TEXMFHOME=~/.local/lib/texmf
export R_LIBS_USER=~/.local/lib/r3.6
# https://doc.qt.io/qt-5/highdpi.html
# https://www.qt.io/blog/2016/01/26/high-dpi-support-in-qt-5-6
export QT_AUTO_SCREEN_SCALE_FACTOR=1
# https://github.com/jupyterlab/jupyterlab/issues/8462
[[ $JUPYTER_SERVER_URL ]] && . ~/.bashrc
